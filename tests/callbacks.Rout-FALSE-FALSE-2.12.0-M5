
R version 3.2.4 (2016-03-10) -- "Very Secure Dishes"
Copyright (C) 2016 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> library(rscala)
> 
> serialize <- as.logical(Sys.getenv("RSCALA_SERIALIZE"))
> output <- as.logical(Sys.getenv("RSCALA_OUTPUT"))
> version <- Sys.getenv("RSCALA_SCALA_VERSION")
> s <- scala(serialize=serialize,stdout=output,stderr=output)
> if ( version != s %~% "scala.util.Properties.versionNumberString" ) stop("Version mismatch.")
> 
> assert <- function(x) {
+   a <<- x
+   if ( ! identical(( s %~% 'R.a._1' ), get("a")) ) stop("Not identical (test 2)")
+   if ( ! identical(( s %~% 'R.get("a")._1' ), get("a")) ) stop("Not identical (test 1)")
+   s$a <- x
+   s %~% 'R.b = a'
+   if ( ! identical(x, s$.R$get("b")$"_1"()) ) stop("Not identical (test 3)")
+ }
> 
> y <- c(0,1,2,3,4,5,6,8)
> for ( x in list(as.integer(y),as.double(y),as.logical(y),as.character(y)) ) {
+   assert(x[1])
+   assert(x[2])
+   assert(x)
+   assert(matrix(x,nrow=1))
+   assert(matrix(x,nrow=2))
+   assert(matrix(x,nrow=4))
+ }
> 
> counter <- 0
> increment <- s$def('','R.eval("counter <<- counter +1")')
> for ( i in 1:10 ) increment()
> if ( counter != 10 ) stop("Counter is off.")
> 
> 
> # Should be a compile-time error because 'ewf' is not defined.
> tryCatch(s %~% '
+   3+4+ewf
+   R.eval("""
+     cat("I love Lisa!\n")
+     a <- "3+9"
+   """)
+ ',error=function(e) e)
<simpleError in scalaEval(interpreter, snippet, interpolate = FALSE): Error in evaluation.>
> s %~% '3+2'
[1] 5
> 
> 
> # Should be an R evaluation error because 'asfd' is not defined and out of place.
> tryCatch(s %~% '
+   3+4
+   R.eval("""
+     cat("I love Lisa!\n")
+     a <- "3+9" asfd
+   """)
+ ',error=function(e) e)
Error in parse(text = snippet) : <text>:4:16: unexpected symbol
3: ")
4:     a <- "3+9" asfd
                  ^
<simpleError in scalaEval(interpreter, snippet, interpolate = FALSE): Error in evaluation.>
> s %~% '3+6'
[1] 9
> 
> 
> myMean <- function(x) {
+   cat("Here is am.\n")
+   mean(x)
+ }
> 
> callRFunction <- s$def('functionName: String, x: Array[Double]','
+   R.xx = x
+   R.eval("y <- "+functionName+"(xx)")
+   R.y._1
+ ')
> 
> tryCatch(callRFunction(1:100),error = function(e) {})
NULL
> callRFunction('myMean',1:100)
Here is am.
[1] 50.5
> 
> # Should be an R evaluation error because 'asfd' is not a package.
> tryCatch(scalaEval(s,'R.eval("library(asdf)")'),error=function(e) e)
Error in library(asdf) : there is no package called ‘asdf’
<simpleError in scalaEval(s, "R.eval(\"library(asdf)\")"): Error in evaluation.>
> s %~% 'R.evalD0("3+4")'
[1] 7
> 
> # Note that callbacks can only be one-level deep.
> s %~% "3+4"
[1] 7
> s %~% 'R.eval("""s %~% "2+3"""")'
> s %~% "3+4"
[1] 7
> 
> 
> 
